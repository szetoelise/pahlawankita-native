package id.irene.a03080112.pahlawankita;

import android.app.Fragment;
import android.content.Intent;
import android.location.Address;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.DetectedActivity;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import geo.GeoObj;
import gl.Color;
import gl.GL1Renderer;
import gl.GLFactory;
import gl.scenegraph.MeshComponent;
import gl.scenegraph.Shape;
import id.irene.a03080112.pahlawankita.R;
import io.nlopez.smartlocation.OnActivityUpdatedListener;
import io.nlopez.smartlocation.OnGeofencingTransitionListener;
import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.OnReverseGeocodingListener;
import io.nlopez.smartlocation.SmartLocation;
import io.nlopez.smartlocation.geofencing.model.GeofenceModel;
import io.nlopez.smartlocation.geofencing.utils.TransitionGeofence;
import io.nlopez.smartlocation.location.providers.LocationGooglePlayServicesProvider;
import system.ArActivity;
import system.DefaultARSetup;
import util.Vec;
import worldData.Entity;
import worldData.Obj;
import worldData.UpdateTimer;
import worldData.Updateable;
import worldData.Visitor;
import worldData.World;

public class MapsActivity extends AppCompatActivity implements OnLocationUpdatedListener,OnMapReadyCallback, OnActivityUpdatedListener, OnGeofencingTransitionListener {

    private TextView mTextMessage;
    private GoogleMap mMap;

    private LocationRequest mLocationRequest;
    private long UPDATE_INTERVAL = 10 * 1000;  /* 10 secs */
    private long FASTEST_INTERVAL = 2000; /* 2 sec */

    private LocationGooglePlayServicesProvider provider;
    private Marker mCurrLocationMarker=null;

    private static final int LOCATION_PERMISSION_ID = 1001;



    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    //mTextMessage.setText(R.string.title_home);
                    findViewById(R.id.map).setVisibility(View.VISIBLE);
                    return true;
                case R.id.navigation_dashboard:
                    findViewById(R.id.map).setVisibility(View.GONE);
                    return true;
                case R.id.navigation_notifications:
                    findViewById(R.id.map).setVisibility(View.GONE);
                    //mTextMessage.setText(R.string.title_notifications);
                    ArActivity.startWithSetup(MapsActivity.this, new DefaultARSetup(){
                        @Override
                        public void addObjectsTo(GL1Renderer renderer,final World world,GLFactory objectsFactory){


                            Location l = new Location("");
                            l.setLatitude(-6.910899);
                            l.setLongitude(107.654944);
                            Obj o = new GeoObj(l);

                            MeshComponent completeMeshWithBothCubes = new Shape();
                            // The composite pattern can be used to combine as many meshes together
                            // as one big mesh as needed:

                            MeshComponent cube = objectsFactory.newCube();
                            // move the 1. cube into a direction in the local object space:
                            Vec localPositionOfCubeInCompleteMesh = new Vec(0, 1, 0);
                            cube.setPosition(localPositionOfCubeInCompleteMesh);
                            completeMeshWithBothCubes.addChild(cube);

                            MeshComponent cube2 = objectsFactory.newCube();
                            // move the 2. cube into another direction in the local object space:
                            Vec localPositionOfCube2InCompleteMesh = new Vec(10, 10, 0);
                            cube2.setPosition(localPositionOfCube2InCompleteMesh);
                            // animations for the submeshes of the group are possible as well:
                            // cube2.addAnimation(anAnimationWhichIsOnlyAppliedOnCube2);
                            completeMeshWithBothCubes.addChild(cube2);

                            // its possible to add any other mesh group or textured mesh etc:
                            // completeMeshWithBothCubes.addChild(anyOtherMeshGroup);

                            // both cubes have no own color attribute so its possible to set the
                            // color of their parent mesh to blue and both children will be rendered
                            // blue too:
                            completeMeshWithBothCubes.setColor(Color.blue());

                            o.setComp(completeMeshWithBothCubes);
                            //world.add(objectsFactory.newHexGroupTest(null));
                            //world.add(objectsFactory.newSquare(Color.blue()));

                            /*
                            Location l = new Location("");
                            l.setLatitude(-6.910899);
                            l.setLongitude(107.654944);
                            Obj o = new GeoObj(l);
                            o.setComp(objectsFactory.newHexGroupTest(null));
                            o.setComp(new Entity() {

                                private Updateable p;
                                private float waitTimeInMilliseconds = 1000;
                                UpdateTimer timer = new UpdateTimer(waitTimeInMilliseconds, null);

                                @Override
                                public boolean accept(Visitor visitor) {
                                    return false;
                                }

                                @Override
                                public boolean update(float timeDelta, Updateable parent) {
                                    p = parent;
                                    if (timer.update(timeDelta, parent)) {
                                        // true once a second, do the calculation here:
                                        float distanceOfGeoObjToUser = Vec.distance(((GeoObj) p)
                                                .getVirtualPosition(), world.getMyCamera()
                                                .getPosition());
                                        // do something with the distance e.g. update a
                                        // corresponding Android UI element
                                    }
                                    return true;
                                }

                                @Override
                                public void setMyParent(Updateable parent) {
                                    this.p = parent;
                                }

                                @Override
                                public Updateable getMyParent() {
                                    return p;
                                }
                            });
                            */
                        }
                    });

                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        LocationManager service = (LocationManager) getSystemService(LOCATION_SERVICE);
        boolean enabled = service
                .isProviderEnabled(LocationManager.GPS_PROVIDER);

        // check if enabled and if not send user to the GSP settings
        // Better solution would be to display a dialog and suggesting to
        // go to the settings
        if (!enabled) {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent);
            Toast.makeText(getApplicationContext(),"Please Enable location services for Pahlawan Kita App",Toast.LENGTH_SHORT).show();
        }


        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }





    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;



        // Add a marker in Sydney and move the camera

        startLocation();
    }

    private void startLocation() {

        provider = new LocationGooglePlayServicesProvider();
        provider.setCheckLocationSettings(true);
        SmartLocation smartLocation = new SmartLocation.Builder(this).logging(true).build();
        smartLocation.location(provider).start(this);
        smartLocation.activity().start(this);

        // Create some geofences
        GeofenceModel mestalla = new GeofenceModel.Builder("1").setTransition(Geofence.GEOFENCE_TRANSITION_ENTER).setLatitude(39.47453120000001).setLongitude(-0.358065799999963).setRadius(500).build();
        smartLocation.geofencing().add(mestalla).start(this);
    }

    private void stopLocation() {
        SmartLocation.with(this).location().stop();
        //locationText.setText("Location stopped!");

        SmartLocation.with(this).activity().stop();
        //activityText.setText("Activity Recognition stopped!");

        SmartLocation.with(this).geofencing().stop();
        //geofenceText.setText("Geofencing stopped!");
    }


    private void showLocation(Location location) {
        if (location != null) {
            final String text = String.format("Latitude %.6f, Longitude %.6f",
                    location.getLatitude(),
                    location.getLongitude());
            //locationText.setText(text);
            LatLng currentloc = new LatLng(location.getLatitude(), location.getLongitude());

            //mMap.addMarker(new MarkerOptions()
            //        .position(sydney)
            //        .title("Marker in Sydney")
            //        .icon(BitmapDescriptorFactory.fromResource(R.drawable.myloc))
            //);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentloc,18));

            if(mCurrLocationMarker!=null){
                mCurrLocationMarker.setPosition(currentloc);
            }else{
                mCurrLocationMarker = mMap.addMarker(new MarkerOptions()
                        .position(currentloc)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.myloc))
                        .title("Kamu disini.")
                        .snippet("0")
                        );
            }

            //tv_loc.append("Lattitude: " + lattitude + "  Longitude: " + longitude);
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentloc, 18));


            Toast.makeText(getApplicationContext(),"Location Changed",Toast.LENGTH_SHORT).show();
            // We are going to get the address for the current position
            SmartLocation.with(this).geocoding().reverse(location, new OnReverseGeocodingListener() {
                @Override
                public void onAddressResolved(Location original, List<Address> results) {
                    if (results.size() > 0) {
                        Address result = results.get(0);
                        StringBuilder builder = new StringBuilder(text);
                        builder.append("\n[Reverse Geocoding] ");

                        List<String> addressElements = new ArrayList<>();
                        for (int i = 0; i <= result.getMaxAddressLineIndex(); i++) {
                            addressElements.add(result.getAddressLine(i));
                        }
                        builder.append(TextUtils.join(", ", addressElements));
                        //locationText.setText(builder.toString());
                    }
                }
            });
        } else {
            //locationText.setText("Null location");
        }
    }

    private void showActivity(DetectedActivity detectedActivity) {
        if (detectedActivity != null) {
            //activityText.setText(
            //        String.format("Activity %s with %d%% confidence",
            //                getNameFromType(detectedActivity),
            //                detectedActivity.getConfidence())
            //);
        } else {
            //activityText.setText("Null activity");
        }
    }

    private void showGeofence(Geofence geofence, int transitionType) {
        if (geofence != null) {
            //geofenceText.setText("Transition " + getTransitionNameFromType(transitionType) + " for Geofence with id = " + geofence.getRequestId());
        } else {
            //geofenceText.setText("Null geofence");
        }
    }

    @Override
    public void onLocationUpdated(Location location) {
        showLocation(location);
    }

    @Override
    public void onActivityUpdated(DetectedActivity detectedActivity) {
        showActivity(detectedActivity);
    }

    @Override
    public void onGeofenceTransition(TransitionGeofence geofence) {
        showGeofence(geofence.getGeofenceModel().toGeofence(), geofence.getTransitionType());
    }

    private String getNameFromType(DetectedActivity activityType) {
        switch (activityType.getType()) {
            case DetectedActivity.IN_VEHICLE:
                return "in_vehicle";
            case DetectedActivity.ON_BICYCLE:
                return "on_bicycle";
            case DetectedActivity.ON_FOOT:
                return "on_foot";
            case DetectedActivity.STILL:
                return "still";
            case DetectedActivity.TILTING:
                return "tilting";
            default:
                return "unknown";
        }
    }

    private String getTransitionNameFromType(int transitionType) {
        switch (transitionType) {
            case Geofence.GEOFENCE_TRANSITION_ENTER:
                return "enter";
            case Geofence.GEOFENCE_TRANSITION_EXIT:
                return "exit";
            default:
                return "dwell";
        }
    }

}
